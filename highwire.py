#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
COPYRIGHT © 2015
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES
ALL RIGHTS RESERVED
 
PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND REDISTRIBUTE
THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY PURPOSE, SO LONG AS THE NAME
OF MICHIGAN STATE UNIVERSITY IS NOT USED IN ANY ADVERTISING OR PUBLICITY
PERTAINING TO THE USE OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC,
WRITTEN PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY OTHER
IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED IN ANY COPY OF ANY
PORTION OF THIS SOFTWARE, THEN THE DISCLAIMER BELOW MUST ALSO BE INCLUDED.
 
THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM MICHIGAN STATE
UNIVERSITY AS TO ITS FITNESS FOR ANY PURPOSE, AND WITHOUT WARRANTY BY
MICHIGAN STATE UNIVERSITY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING
WITHOUT LIMITATION THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES SHALL
NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM ARISING OUT OF OR IN
CONNECTION WITH THE USE OF THE SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 
Written by Devin Higgins, 2015
(c) Michigan State University Board of Trustees
Licensed under GNU General Public License (GPL) Version 2.
"""
import requests
from bs4 import BeautifulSoup
import os
import time
from pprint import pprint
from urlparse import urljoin
import sys
import urllib2
#from PyPDF2 import PdfFileReader


class HighWire():

    def __init__(self, base_url="http://ps.oxfordjournals.org/content/by/year", domain="http://ps.oxfordjournals.org/"):

        self.base_url = base_url
        self.domain = domain


    def __make_dir(self, new_dir):

        if not os.path.isdir(new_dir):
            os.mkdir(new_dir)


    def __open_url(self):

        headers = {"User-Agent": self.user_agent}

        r = requests.get(self.url, headers=headers)
        if r.ok:
            html = r.content
            self.returned_url = r.url
            print "[{0}]: Found URL: {1}".format(r.status_code, self.url)

        else:
            print "[{0}]: Failed to return: {1}".format(r.status_code, self.url)
            html = None

        time.sleep(self.sleep_seconds)
        return html

    def __get_years(self):
        self.url = self.base_url
        self.available_years = []
        html = self.__open_url()
        if html is not None:
            soup = BeautifulSoup(html)
            for year in soup.find_all(class_="proxy-archive-year"):
                if len(year.get_text()) == 4:
                    self.available_years.append(year.get_text())
                    
            print "Found {0} years of journals".format(len(self.available_years))
            self.__match_years()

        else:
            print "No valid HTML returned"


    def __match_years(self):

        if self.requested_years == "all":
            self.years = self.available_years
        elif not isinstance(self.requested_years, list):
            self.years = []
        else:
            self.years = list(set(self.requested_years).intersection(self.available_years))

    def __get_issues(self, html, year):

        soup = BeautifulSoup(html)
        for month in soup.find_all(class_="proxy-archive-by-year-month"):
            for issue in month.find_all("a"):
                self.issue_date = issue.get_text()
                self.issue_date_dir = self.issue_date.strip().replace(" ", "_")
                self.current_dir = os.path.join(self.output_dir, self.output_ns, self.year, self.issue_date_dir)
                self.__make_dir(self.current_dir)
                self.issues[year].append(self.issue_date)
                self.url = urljoin(self.domain, issue.get('href'))
                html = self.__open_url()
                if html is not None:
                    print "Found Journal Issues..."
                    self.__get_articles(html)

    def __get_articles(self, html):

        soup = BeautifulSoup(html)

        for section in soup.find_all(class_="level1"):
            if section.find('h2'):
                self.section_name = section.find('h2').get('id')
            else:
                self.section_name = "UnnamedSection"
            self.current_dir = os.path.join(self.output_dir, self.output_ns, self.year, self.issue_date_dir, self.section_name)
            self.__make_dir(self.current_dir)
            self.__get_content(section)

        

    def __get_content(self, section):

        for item in section.find_all('li', class_="toc-cit"):
            self.title = item.find('h4').get_text()
            for link in item.find_all('a'):
                self.link = link
                self.rel_value = link.get('rel')
                if self.rel_value:
                    if "full-text" in self.rel_value:
                        self.__download_file("html")
                    """
                    elif "full-text.pdf" in self.rel_value:
                        self.__download_file("pdf")
                    """

    def __download_file(self, filetype):

        print "Found full-text..."
        self.count += 1
        self.url = urljoin(self.domain, self.link.get('href'))
        self.file_location = os.path.join(self.current_dir, self.title.replace(":", "-").replace(" ", "_").replace("/", "-")[:100]+"."+filetype)
        try:
            if filetype == "html":
                data = self.__open_url()  
                self.soup = BeautifulSoup(data)
                #data = self.__open_url()    
                with open(self.file_location, "w") as f:
                    f.write(data)
                    print "Wrote file at {0}".format(self.file_location.encode('utf8', 'replace'))

            elif filetype=="pdf":
                rq = urllib2.Request(self.url)
                res = urllib2.urlopen(rq)
                time.sleep(self.sleep_seconds)
                with open (self.file_location, "wb") as f:
                    f.write(res.read())

            if self.extract_full_text:
                if filetype == "html":
                    self.__get_text_from_html()

                elif filetype == "pdf":
                    self.__get_text_from_pdf()

        except Exception as e:
            print "Text extraction failed for {0}, {1}, {2}".format(self.url, filetype, e)
            self.failed_urls.append((self.year, self.url))

    def __get_text_from_html(self):

        full_text = self.soup.find(class_="fulltext-view").get_text()
        full_text_file_location = self.file_location.replace(".html", ".txt")
        with open(full_text_file_location, "w") as f:
            f.write(full_text.encode("UTF-8"))
            print "Copied full text to {0}".format(full_text_file_location.encode('utf8', 'replace'))

    def __get_text_from_pdf(self):

        full_text = ""
        full_text_file_location = self.file_location.replace(".pdf", ".txt")
        with open(self.file_location, "rb") as f:
            a = PdfFileReader(f)
            for i in range(a.getNumPages()):
                full_text += a.getPage(i).extractText()
        with open(full_text_file_location, "w") as f:
            f.write(full_text.encode("UTF-8"))
            print "Copied full text to {0}".format(full_text_file_location.encode('utf8', 'replace'))


    def get_content(self, user_agent, output_dir=".", requested_years="all", extract_full_text=True, sleep_seconds=1):

        self.sleep_seconds=sleep_seconds
        self.extract_full_text = extract_full_text
        self.user_agent = user_agent
        self.requested_years = requested_years
        self.count = 0
        self.issues = {}
        self.failed_urls = []
        self.output_dir = output_dir
        self.output_ns = "HighWire_Output"
        self.current_dir = os.path.join(self.output_dir, self.output_ns)
        self.__make_dir(self.current_dir)
        self.__get_years()
  
        for year in self.years:
            self.issues[year] = []
            self.year = year
            self.current_dir = os.path.join(self.output_dir, self.output_ns, self.year)
            self.__make_dir(self.current_dir)
            self.url = os.path.join(self.base_url, year)
            html = self.__open_url()
            if html is not None:
                "Retrieving Issues for {0}".format(year)
                self.__get_issues(html, year)

        print "Returned {0} items".format(self.count)
        pprint(self.issues)

